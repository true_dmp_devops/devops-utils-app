# Applications
+ devops-harden.py - Application to send the hardening script to remote host and execute remotely.
+ qualys-check.py - Check if Qualys service is still running and als check if Qualys is installed.

# Prerequisites
+ plink - https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
+ pscp - https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
+ python - https://www.python.org/downloads/ (version 3.8 or above)
+ git bash - https://git-scm.com/downloads

# How to run devops-harden.py
1. Clone the repository to local workspace and change directory to your workspace.
2. Run the command by executing the command below.
    ```
    export SSH_PASSWORD=<this_is_your_secret>
    ./devops-harden.py -u=<user> -pw=${SSH_PASSWORD} <remotehost-ip>
    ```
3. We strongly recommend to temporary keep the password in the environment variable and use it in the command line when calling ./devops-harden.py. 
4. In order to run the hardening script directly on the shell so please use the command below.
    ```bash
    export FILE_HASH=c34c06cfc9fc9497f4fdc9383a76468219670c15 #This can be changed per git commit.
    curl https://bitbucket.org/seubpong/devops-utils-app/raw/${FILE_HASH}/configs/hardening/hardening.pl | sudo perl
    ```

# How to run qualys-check.py
1. Clone the repository to local workspace and change directory to your workspace.
2. Run the command by executing the command below.
    ```
    export SSH_PASSWORD=<this_is_your_secret>
    ./qualys-check.py -u=<user> -pw=${SSH_PASSWORD} <remotehost-ip>
    ```
3. We strongly recommend to temporary keep the password in the environment variable and use it in the command line when calling ./qualys-check.py. 