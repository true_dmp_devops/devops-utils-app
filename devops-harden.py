#!/usr/bin/env python

import sys
import importlib
import argparse
import version
from src.devops.applications.hardening.app import HardeningApp

def main():
    print("Application version [{}]\n".format(version.VERSION))
    desc = 'Send the hardening script to host and run it remotely'

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("host", help="Remote host IP or name.")
    parser.add_argument("-p", "--port", default="22", help="Remote host port (default is 22).")
    parser.add_argument("-u", "--user", default="", help="SSH user to remote host.")
    parser.add_argument("-pw", "--password", default="", help="SSH password to remote host.")
    args = parser.parse_args()

    class_map = {
        'HardeningApp' : 'src.devops.applications.hardening.app'
    }

    cmd = 'HardeningApp'

    module = importlib.import_module(class_map[cmd])
    classname = getattr(module, cmd)
    app = classname()
    app.run(args)

if __name__ == "__main__":
    main()
