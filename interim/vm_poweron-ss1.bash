#!/bin/bash

#Name:                    D2D-TempHardenigTestA01
#BIOS UUID:               42315061-bcd2-2dd0-b525-7f6b17c16211

#Name:                    D2D-TempHardenigTestA02
#BIOS UUID:               423108fc-b7f8-acde-089e-e242388e60c6

USER=${DEVOPS_USERNAME}
PASSWORD=${DEVOPS_PASSWORD}
V_HOST=${VCENTER_HOST}
V_USER=${VCENTER_USER}
V_PASSWORD=${VCENTER_PASSWORD}

NOTI_URL=https://notify-api.line.me/api/notify
TOKEN=geplFQ3esmEKr69ZxVQIJO3gIDGoEd8s3a6mkXEgNiX

#Payment
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 4231b632-44ea-0ad7-e34a-5d3fc89f10f4
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423197c0-d96e-2435-e827-b43414595e0a
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423118ea-a6f6-891a-4f6a-0e2f6f01e50b
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 42315763-d692-8ca9-cd75-7c81d7a0efa5
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 4231c59b-c114-9632-5033-df804fa95d18
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 4231df8b-0643-e311-e785-976ea11f1a8a
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423107d1-fbde-04fd-156f-1798e21828de
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423141e8-508c-91e5-8ced-d7377884c2d0
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423108e1-085b-657b-9739-b53df0f27adf
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423170a3-8b9b-b8bc-4fe8-0157d5eb9459
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 423197d0-591a-ea96-82b1-2f19d76f7d92
./devops-vm-poweron.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} 42315ca8-bfd1-4d98-4dc6-5d5d2a1e3c15

curl -s -X POST -H "Authorization: Bearer ${TOKEN}" \
-F "message=DONE : Snapshot(s) created successfully!!!" \
${NOTI_URL}