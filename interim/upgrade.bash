#!/bin/bash

#./interim/upgrade.bash 10.198.105.95

USER=${DEVOPS_USERNAME}
PASSWORD=${DEVOPS_PASSWORD}
IP=10.198.105.96
V_HOST=${VCENTER_HOST}
V_USER=${VCENTER_USER}
V_PASSWORD=${VCENTER_PASSWORD}

NOTI_URL=https://notify-api.line.me/api/notify
TOKEN=geplFQ3esmEKr69ZxVQIJO3gIDGoEd8s3a6mkXEgNiX

if [ $# -gt 0 ]; then
    IP=$1

    curl -s -X POST -H "Authorization: Bearer ${TOKEN}" \
    -F "message=START - Hardening+Patching [${IP}]..." \
    ${NOTI_URL}

#    echo ""
#    echo "############## INFO : Creating snapshot..."
#    ./devops-vm-snapshot.py -u=${USER} -pw=${PASSWORD} -vh=${V_HOST} -vu=${V_USER} -vp=${V_PASSWORD} ${IP}

    if [ $? -eq 0 ]
    then
        echo "############## INFO : Patching might take long time..."
        ./devops-patch-update.py -u=${USER} -pw=${PASSWORD} ${IP}


        echo "############## INFO : Hardening..."
        ./devops-harden.py -u=${USER} -pw=${PASSWORD} ${IP}


        curl -s -X POST -H "Authorization: Bearer ${TOKEN}" \
        -F "message=END - Hardening+Patching [${IP}], don't forget to stop and start VM before validation!!!" \
        ${NOTI_URL}
    else
        exit 1
    fi
else
    echo "ERROR : Require an IP address as a parameter!!!"
fi