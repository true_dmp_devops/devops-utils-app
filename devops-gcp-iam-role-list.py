#!/usr/bin/env python

import sys
import importlib
import argparse
import version
from src.devops.applications.gcp.app_iam_role_list import ProjectIamRoleListApp

def main():
    print("Application version [{}]\n".format(version.VERSION))
    desc = 'Retrieve IAM role list for current user active projects'

    parser = argparse.ArgumentParser(description=desc)
    args = parser.parse_args()

    class_map = {
        'ProjectIamRoleListApp' : 'src.devops.applications.gcp.app_iam_role_list'
    }

    cmd = 'ProjectIamRoleListApp'

    module = importlib.import_module(class_map[cmd])
    classname = getattr(module, cmd)
    app = classname()
    app.run(args)

if __name__ == "__main__":
    main()
