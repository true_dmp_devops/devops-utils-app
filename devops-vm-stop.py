#!/usr/bin/env python

import sys
import importlib
import argparse
import version
import atexit
import time
import os
from pyVmomi import vim
from pyVmomi import vmodl
from pyVim import connect
from datetime import datetime

from src.devops.applications.vcenter import tasks
from src.devops.applications.hardening.app import HardeningApp
from src.devops.applications.commons.commands.command_line_noti import LineNotifyCommand

def endit():
    """
    times how long it took for this script to run.

    :return:
    """
    pass

def get_service_instance(vc_host, vc_user, vc_passwd, vc_port):
    si = None
    try:
        si = connect.SmartConnectNoSSL(
            host=vc_host,
            user=vc_user,
            pwd=vc_passwd,
            port=vc_port)

        atexit.register(connect.Disconnect, si)
        atexit.register(endit)
    except:
        pass

    return si

def get_vm_by_ip(service_instance, ip):
    vm = service_instance.content.searchIndex.FindByIp(None, ip, True)
    return vm

def power_off(si, vm):
    if vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:        
        poweroff_task = vm.PowerOff()
        tasks.wait_for_tasks(si, [poweroff_task])            

def main():
    print("Application version [{}]\n".format(version.VERSION))    
    desc = 'Restart VM by power off first and then power on at the end'
    
    print("Waiting for 10 seconds...", flush=True)
    time.sleep(10)

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("host", help="Remote host IP or name.")
    parser.add_argument("-p", "--port", default="22", help="Remote host port (default is 22).")
    parser.add_argument("-u", "--user", default="", help="SSH user to remote host.")
    parser.add_argument("-pw", "--password", default="", help="SSH password to remote host.")
    parser.add_argument("-vh", "--vhost", default="", help="VCenter host IP address.")
    parser.add_argument("-vu", "--vuser", default="", help="VCenter username.")
    parser.add_argument("-vp", "--vpasswd", default="", help="VCenter password.")
    args = parser.parse_args()

    si = get_service_instance(args.vhost, args.vuser, args.vpasswd, 443)

    if not si:
        raise SystemExit("Unable to connect to host with supplied info.")    

    vm = get_vm_by_ip(si, args.host)

    if not vm:
        raise SystemExit("Unable to locate VM [{0}], please make sure VM is power on!!!".format(args.host))

    print("Powering off for [{0}]...".format(args.host), flush=True)
    power_off(si, vm)
    print("Power is off now for [{0}]".format(args.host), flush=True)

    #print("Powering on for [{0}]...".format(args.host), flush=True)
    #power_on(vm)
    #print("Power is on now for [{0}]".format(args.host), flush=True) 

if __name__ == "__main__":
    main()
