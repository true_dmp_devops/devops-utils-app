from src.devops.applications.commons.base_gcp_application import BaseGcpApplication
from src.devops.applications.commons.commands.cmd_gcp_project_get_iam_policy import GcpProjectGetIamPolicyCommand
from src.devops.applications.commons.commands.cmd_gcp_project_list import GcpProjectListCommand

from collections import namedtuple
import json
import re

class ProjectIamRoleListApp(BaseGcpApplication):

    def run(self, prm):

        projs = GcpProjectListCommand()
        status, result = projs.run(prm)
        
        if (status != 0):
            #Try again
            print("GcpProjectListCommand() failed with [{}] status!!!".format(status))
            return

        result = re.sub(r'"(.+)\-(.+)"\:', '"error":', result) #Replace field name contains dash with "error"
        x = json.loads(result, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

        cnt = len(x)
        for j in range(cnt):
            obj = x[j]
            print("#######", flush=True)
            print("PROJECT|{}|{}|{}".format(obj.projectId, obj.name, obj.projectNumber), flush=True)

            iam = GcpProjectGetIamPolicyCommand()
            prm.project = obj.projectId
            status, rs = iam.run(prm)

            o = json.loads(rs, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

            bindings = []
            if hasattr(o, 'bindings'):
                bindings = o.bindings

            for k in range(len(bindings)):
                role = bindings[k]
                print("ROLE|{}".format(role.role), flush=True)

                members = role.members
                for m in range(len(members)):
                    member = members[m]
                    print("MEMBER|{}".format(member), flush=True)
        
        return
