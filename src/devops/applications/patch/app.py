from src.devops.applications.commons.base_application import BaseApplication

class PatchYumUpdateApp(BaseApplication):
    def __get_execute_command__(self, prm):
        return 'sudo -S bash /tmp/patch.bash'

    def __get_local_script_file__(self, prm):
        return 'configs/patch/patch.bash'

    def __get_extra_files_list__(self, prm):
        extras = []
        return extras
