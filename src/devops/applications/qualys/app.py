from src.devops.applications.commons.base_application import BaseApplication

class QualysInstallApp(BaseApplication):
    def __get_execute_command__(self, prm):
        return 'sudo -S bash /tmp/qualys_install.bash'

    def __get_local_script_file__(self, prm):
        return 'configs/qualys/qualys_install.bash'

    def __get_extra_files_list__(self, prm):
        extras = ['configs/qualys/qualys-cloud-agent.x86_64.rpm']
        return extras


class QualysCheckApp(BaseApplication):
    def __get_execute_command__(self, prm):
        return 'sudo -S bash /tmp/qualys_check.bash'

    def __get_local_script_file__(self, prm):
        return 'configs/qualys/qualys_check.bash'

    def __get_extra_files_list__(self, prm):
        extras = []
        return extras
