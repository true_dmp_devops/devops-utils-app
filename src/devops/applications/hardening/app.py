from src.devops.applications.commons.base_application import BaseApplication

class HardeningApp(BaseApplication):
    def __get_execute_command__(self, prm):
        return 'sudo -S perl /tmp/hardening.pl'

    def __get_local_script_file__(self, prm):
        return 'configs/hardening/hardening.pl'

    def __get_extra_files_list__(self, prm):
        extras = []
        return extras
