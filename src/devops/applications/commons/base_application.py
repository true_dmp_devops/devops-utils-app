import re
from src.devops.applications.commons.commands.command_pscp import PscpCommand
from src.devops.applications.commons.commands.command_plink import PlinkCommand

class BaseApplication():
    def get_host_key(self, result):
        hostkey = ''
        #ssh-ed25519 255 e5:e9:4d:2c:7b:33:67:8c:34:51:b6:f1:77:27:2f:7f

        for line in result.splitlines():
            if m := re.match(r"ssh-(.+)\s255\s(.+)", line):
                hostkey = m.group(2)

        return hostkey

    def __get_execute_command__(self, prm):
        return 'echo "Please override this function"'

    def __get_local_script_file__(self, prm):
        return 'please_override_this.txt'

    def __get_extra_files_list__(self, prm):
        extras = []
        return extras

    def run(self, prm):
        host = prm.host

        custom_cmd = self.__get_execute_command__(prm)
        cmd = 'echo {} | {}'.format(prm.password, custom_cmd)

        local_script_file = self.__get_local_script_file__(prm)

        param = {
            'USER': prm.user,
            'HOST': host,
            'PORT': prm.port,
            'LOCAL_FILE': local_script_file,
            'REMOTE_DIR': "/tmp",
            'COMMAND': cmd,
            'PASSWORD': prm.password,
        }

        print("Running scp command to [{}]".format(host))

        scp1 = PscpCommand()
        status, result = scp1.run(param)
        print("Done executing 1st scp with status [{}] [{}]".format(status, result))

        if (status != 0):
            #Get the hostkey here
            hostkey = self.get_host_key(result)
            param['HOSTKEY'] = hostkey

            #Try again
            print("Trying with hostkey [{}]".format(hostkey))
            scp2 = PscpCommand()
            status, result = scp2.run(param)
            print("Done executing 2nd scp with status [{}] [{}]".format(status, result))

        extra_files = self.__get_extra_files_list__(prm)
        for extra_file in extra_files:
            param['LOCAL_FILE'] = extra_file
            scp3 = PscpCommand()
            status, result = scp3.run(param)
            print("Done executing 3rd scp with status [{}] [{}]".format(status, result))

        print("Running remote script")
        df = PlinkCommand()
        status, result = df.run(param)
        print("Done executing ssh with status [{}] {}".format(status, result))

        return result
