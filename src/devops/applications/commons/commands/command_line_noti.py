from src.devops.applications.commons.commands.command import Command

class LineNotifyCommand(Command):
    def __construct_command__(self, param):

        token = param['TOKEN']
        message = param['MESSAGE']

        auth_header = 'Authorization: Bearer {0}'.format(token)
        body = 'message={0}'.format(message)

        arrs = [
            'curl',
            '-s',
            '-X',
            'POST',
            '-H',
            auth_header,
            '-F',
            body,
            'https://notify-api.line.me/api/notify'       
        ]

        return arrs
