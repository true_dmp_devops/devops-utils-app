from src.devops.applications.commons.commands.command import Command

class ScpCommand(Command):
    def __construct_command__(self, param):

        user = param['USER']
        host = param['HOST']
        port = param['PORT']
        local_file = param['LOCAL_FILE']
        remote_dir = param['REMOTE_DIR']        

        remote_path = "{}@{}:{}".format(user, host, remote_dir)

        sshpass_arr = []
        if 'PASSWORD' in param:
            password = param['PASSWORD']
            sshpass_arr = [
                'sshpass',
                '-p',
                password
            ]    

        arrs = [
            'scp',
            '-P',
            port,
            '-o',
            'StrictHostKeyChecking=no',
            local_file,
            remote_path,            
        ]

        sshpass_arr.extend(arrs)  ## add list of elems at end

        return sshpass_arr
