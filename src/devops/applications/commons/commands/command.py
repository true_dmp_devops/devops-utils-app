import os
import subprocess
import sys

class Command():
    def __construct_command__(self, param):
        return ""

    def run(self, param):
        arrs = self.__construct_command__(param)

        is_windows = sys.platform.startswith('win')
      
        proc = subprocess.Popen(arrs,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE,
            shell = is_windows #Only use shell in Windows
        )

        result, err_msg = proc.communicate()
        status = proc.returncode

        if (status != 0):
            return status, err_msg.decode('utf8')

        return status, result.decode('utf8')
