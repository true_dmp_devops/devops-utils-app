from src.devops.applications.commons.commands.command import Command

class PlinkCommand(Command):
    def __construct_command__(self, param):

        user = param['USER']
        host = param['HOST']
        port = param['PORT']
        command = param['COMMAND'] 
        passwd = param['PASSWORD']  

        remote_path = "{}@{}".format(user, host)    

        arrs = [
            'plink',
            '-v',
            '-P',
            port,
            '-pw',
            passwd,
            '-batch',
            remote_path,
            command,
        ]

        if 'HOSTKEY' in param:
            arrs.insert(7, '-hostkey')
            arrs.insert(8, param['HOSTKEY'])

        return arrs
