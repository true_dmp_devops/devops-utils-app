from src.devops.applications.commons.commands.command import Command

class SshExecCommand(Command):
    def __construct_command__(self, param):

        user = param['USER']
        host = param['HOST']
        port = param['PORT']
        command = param['COMMAND']   

        remote_path = "{}@{}".format(user, host)    

        sshpass_arr = []
        if 'PASSWORD' in param:
            password = param['PASSWORD']
            sshpass_arr = [
                'sshpass',
                '-p',
                password
            ]    

        arrs = [
            'ssh',
            '-t',
            '-p',
            port,
            '-o',
            'StrictHostKeyChecking=no',
            remote_path,
            command,
        ]

        sshpass_arr.extend(arrs)  ## add list of elems at end

        return sshpass_arr
