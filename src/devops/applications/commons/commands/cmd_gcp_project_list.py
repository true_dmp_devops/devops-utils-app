from src.devops.applications.commons.commands.command import Command

class GcpProjectListCommand(Command):
    def __construct_command__(self, param):
        arrs = [
            'gcloud',
            'projects',
            'list',
            '--format=json'
        ]

        return arrs
