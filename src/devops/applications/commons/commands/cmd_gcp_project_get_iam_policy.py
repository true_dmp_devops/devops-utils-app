from src.devops.applications.commons.commands.command import Command

class GcpProjectGetIamPolicyCommand(Command):
    def __construct_command__(self, param):
        project = param.project

        arrs = [
            'gcloud',
            'projects',
            'get-iam-policy',
            project,
            '--format=json'
        ]
        #gcloud projects get-iam-policy truedigital-trueyou-vendor --format=json
        
        return arrs
