from src.devops.applications.commons.commands.command import Command

class PscpCommand(Command):
    def __construct_command__(self, param):

        user = param['USER']
        host = param['HOST']
        port = param['PORT']
        local_file = param['LOCAL_FILE']
        remote_dir = param['REMOTE_DIR']
        passwd = param['PASSWORD']     

        remote_path = "{}@{}:{}".format(user, host, remote_dir)

        arrs = [
            'pscp',
            '-P',
            port,
            '-pw',            
            passwd,       
            '-batch',
            local_file,
            remote_path,                        
        ]

        if 'HOSTKEY' in param:
            arrs.insert(6, '-hostkey')
            arrs.insert(7, param['HOSTKEY'])

        return arrs
