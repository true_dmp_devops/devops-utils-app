#!/usr/bin/env python

import sys
import importlib
import argparse
import version
import atexit
import time
import os
from pyVmomi import vim
from pyVmomi import vmodl
from pyVim import connect
from datetime import datetime

from src.devops.applications.vcenter import tasks
from src.devops.applications.hardening.app import HardeningApp
from src.devops.applications.commons.commands.command_line_noti import LineNotifyCommand

def endit():
    """
    times how long it took for this script to run.

    :return:
    """
    pass

def get_service_instance(vc_host, vc_user, vc_passwd, vc_port):
    si = None
    try:
        si = connect.SmartConnectNoSSL(
            host=vc_host,
            user=vc_user,
            pwd=vc_passwd,
            port=vc_port)

        atexit.register(connect.Disconnect, si)
        atexit.register(endit)
    except:
        pass

    return si

def get_vm_by_ip(service_instance, ip):
    vm = service_instance.content.searchIndex.FindByIp(None, ip, True)
    return vm

def list_vm_snapshot(vm):
    snap_info = vm.snapshot

    tree = snap_info.rootSnapshotList
    while tree[0].childSnapshotList is not None:
        print("Snapshot: {0} => {1}".format(tree[0].name, tree[0].description))
        if len(tree[0].childSnapshotList) < 1:
            break
        tree = tree[0].childSnapshotList

    return


def answer_vm_question(virtual_machine):
    choices = virtual_machine.runtime.question.choice.choiceInfo
    default_option = None
    if virtual_machine.runtime.question.choice.defaultIndex is not None:
        ii = virtual_machine.runtime.question.choice.defaultIndex
        default_option = choices[ii]
    choice = None
    while choice not in [o.key for o in choices]:
        print("VM power on is paused by this question:\n\n")
        print("\n".join(textwrap.wrap(
            virtual_machine.runtime.question.text, 60)))
        for option in choices:
            print("\t %s: %s " % (option.key, option.label))
        if default_option is not None:
            print("default (%s): %s\n" % (default_option.label,
                                          default_option.key))
        choice = raw_input("\nchoice number: ").strip()
        print("...")
    return choice

def power_on(vm):
    if vm.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
        # now we get to work... calling the vSphere API generates a task...
        pwo_task = vm.PowerOn()

        # We track the question ID & answer so we don't end up answering the same
        # questions repeatedly.
        answers = {}
        while pwo_task.info.state not in [vim.TaskInfo.State.success,
                                  vim.TaskInfo.State.error]:

            # we'll check for a question, if we find one, handle it,
            # Note: question is an optional attribute and this is how pyVmomi
            # handles optional attributes. They are marked as None.
            if vm.runtime.question is not None:
                question_id = vm.runtime.question.id
                if question_id not in answers.keys():
                    answers[question_id] = answer_vm_question(vm)
                    vm.AnswerVM(question_id, answers[question_id])
            return    

def power_off(si, vm):
    if vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:        
        poweroff_task = vm.PowerOff()
        tasks.wait_for_tasks(si, [poweroff_task])        
    
def create_vm_snapshot(vm, name, desc):
    task = vm.CreateSnapshot_Task(
        name=name,
        description=desc,
        memory=True,
        quiesce=False)

    return task

def main():
    print("Application version [{}]\n".format(version.VERSION))    
    desc = 'Taking VM snapshot by power off first and then power on at the end'

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("host", help="Remote host IP or name.")
    parser.add_argument("-p", "--port", default="22", help="Remote host port (default is 22).")
    parser.add_argument("-u", "--user", default="", help="SSH user to remote host.")
    parser.add_argument("-pw", "--password", default="", help="SSH password to remote host.")
    parser.add_argument("-vh", "--vhost", default="", help="VCenter host IP address.")
    parser.add_argument("-vu", "--vuser", default="", help="VCenter username.")
    parser.add_argument("-vp", "--vpasswd", default="", help="VCenter password.")
    args = parser.parse_args()

    si = get_service_instance(args.vhost, args.vuser, args.vpasswd, 443)

    if not si:
        raise SystemExit("Unable to connect to host with supplied info.")    

    vm = get_vm_by_ip(si, args.host)

    if not vm:
        raise SystemExit("Unable to locate VM [{0}], please make sure VM is power on!!!".format(args.host))

    print("Powering off for [{0}]...".format(args.host), flush=True)
    power_off(si, vm)
    print("Power is off now for [{0}]".format(args.host), flush=True)

    dtm = datetime.now()
    timestamp = "{0}{1:02d}{2:02d}_{3:02d}{4:02d}".format(dtm.year, dtm.month, dtm.day, dtm.hour, dtm.minute)
    snapshot_name = "SNAPSHOT-{0}".format(timestamp)

    print("Creating snapshot [{0}] for [{1}]...".format(snapshot_name, args.host), flush=True)
    snapshot_task = create_vm_snapshot(vm, snapshot_name, snapshot_name) 
    tasks.wait_for_tasks(si, [snapshot_task])
    print("Listing current snapshot(s)...", flush=True)
    list_vm_snapshot(vm)
    print("Created snapshot [{0}] for [{1}] successfully.".format(snapshot_name, args.host), flush=True)


    print("Powering on for [{0}]...".format(args.host), flush=True)
    power_on(vm)
    print("Power is on now for [{0}]".format(args.host), flush=True)

    print("Waiting for 20 seconds for sshd to start...", flush=True)
    time.sleep(20)    

if __name__ == "__main__":
    main()
