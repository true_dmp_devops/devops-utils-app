#!/bin/bash

systemctl stop qualys-cloud-agent

chmod +x /tmp/qualys-cloud-agent.x86_64.rpm
rpm -ivh /tmp/qualys-cloud-agent.x86_64.rpm

/usr/local/qualys/cloud-agent/bin/qualys-cloud-agent.sh \
ActivationId=b747a5e3-ff1c-4e64-b25e-a9b35ac8c77c \
CustomerId=585d2f5c-30c1-d1a9-8186-6d0ba12eb207

systemctl start qualys-cloud-agent
