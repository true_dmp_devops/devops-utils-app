#!/bin/bash

echo ""
echo "#### Listing process with keyword 'qualys-cloud-agent' ####"
ps -ef | grep qualys-cloud-agent | grep -v grep

echo ""
echo "#### Listing program files in '/usr/local/qualys/cloud-agent/bin' ####"
ls -lrt /usr/local/qualys/cloud-agent/bin

echo ""
echo "#### Listing log files in '/var/log/qualys' ####"
ls -lrt /var/log/qualys


PS_COUNT=$(ps -ef | grep qualys-cloud-agent | grep -v grep | wc -l)
HOST=$(hostname)
EXE_COUNT=$(ls -lrt /usr/local/qualys/cloud-agent/bin | grep -v '^total' | wc -l)
LOG_COUNT=$(ls -lrt /var/log/qualys | grep -v '^total' | wc -l)

echo ""
echo "HOST=$HOST|PROCESS_COUNT=$PS_COUNT|EXECUTABLE_COUNT=$EXE_COUNT|LOG_COUNT=$LOG_COUNT"

exit 0
