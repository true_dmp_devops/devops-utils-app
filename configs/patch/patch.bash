#!/bin/bash

#This command will not work for CentOs : yum --security update -y

#Kafka - will not need to upgrade java version

yum --exclude=java* \
--exclude=mysql-cluster-* \
--exclude=mysql-* \
--exclude=mariadb-* \
--exclude=mongodb-* \
--exclude=elasticsearch* \
--exclude=kibana* \
--exclude=zabbix* \
--exclude=logstash* \
--exclude=filebeat-* \
update -y

exit 0
