#!/bin/perl

#./hardening.pl <profile>

use File::Copy;
use strict;
use warnings;
use POSIX qw/strftime/;
use File::Basename;

my $PROFILE="ALLOW_TMP_EXEC";
my $num_args = @ARGV;

if ($num_args >= 1) 
{
    $PROFILE = $ARGV[0];     
}

print "\nUsing profile [$PROFILE] [$num_args]\n";   
my $BACKUP_DIR = "";

intro();

modprob();
tmp_partition();
fstab_update();
fstab_add();
grub();
core_dump();
sysctl();
sysctl_adjust();
deny_host();
iptables_drop();
grub_audit();
var_log_permission();
audit_rule();
rsys_log();
cron();
sshd();
sshd_adjust();
passwd_policy();
authen1('/etc/pam.d/system-auth');
authen1('/etc/pam.d/password-auth');
authen2();
authen3();

exit(0);

sub is_default_profile
{
    if (($PROFILE eq '') || ($PROFILE eq 'DEFAULT'))
    {
        return 1;
    }

    return 0;
}

sub is_tmp_exec_allow_profile
{
    if (($PROFILE eq 'KAFKA') || ($PROFILE eq 'MONGO') || ($PROFILE eq 'MONGO') || ($PROFILE eq 'ALLOW_TMP_EXEC'))
    {
        return 1;
    }

    return 0;
}

sub intro
{
    my $timestamp = strftime('%Y%m%d%H%M%S', localtime);
    $BACKUP_DIR = "/tmp/$timestamp";

    chomp(my ($server_name) = `hostname | cut -f 1 -d.`);
    print("\n\nStart hardening server [$server_name]...\n");

    print("Creating backup director [$BACKUP_DIR]...\n");
    mkdir($BACKUP_DIR, 0755);
}

sub deny_host
{    
    my $cfg_file = '/etc/hosts.deny';

    if (-e $cfg_file) 
    {
        print("File [$cfg_file] already exist so delete it.\n");
        unlink($cfg_file);
    }
    else
    {
        print("File [$cfg_file] does not exist, so nothing to do.\n");
    }
}

sub modprob
{    
    my ($dry_run_flag) = @_;
    my $cfg_file = '/etc/modprobe.d/CIS.conf';

    my $content = <<'END_MESSAGE';
install cramfs /bin/true
install freevxfs /bin/true
install jffs2 /bin/true
install hfs /bin/true
install hfsplus /bin/true
install squashfs /bin/true
install udf /bin/true
install vfat /bin/true
END_MESSAGE

    if (-e $cfg_file) 
    {
        print("File [$cfg_file] already exist so delete it first then create the new one...\n");
        unlink($cfg_file);
    }
    
    create_text_file($cfg_file, $content);
    print("Created file [$cfg_file]\n");
}

sub tmp_partition
{
    execute_command('systemctl unmask tmp.mount');
    execute_command('systemctl enable tmp.mount');

    #This file should be automatically created after the commands above.
    my $cfg = '/etc/systemd/system/local-fs.target.wants/tmp.mount';

    #Allow /tmp executable for all cases
    my $option = 'mode=1777,nodev,nosuid,strictatime';

    my %patterns = (
        '^Options=(.+)$' => $option,
    );    

    update_if_not_exist($cfg, \%patterns, 'replace');
}

sub authen2
{
    execute_command('useradd -D -f 120');
    
    my %update_pattern_map = (
        '^PASS_MAX_DAYS\s+(.*?)\s*$' => '365',
        '^PASS_MIN_DAYS\s+(.*?)\s*$' => '0',
    );

    update_if_not_exist('/etc/login.defs', \%update_pattern_map, 'replace');

    my @users = get_shellable_users();    
    foreach my $user (@users)
    {
        execute_command("chage --inactive 120 $user");
        #execute_command("chage -M 365 $user"); #ISSUE - user will not be able to login immediately
        execute_command("chage -m 7 $user");
        
        print("\n");
    }
}

sub authen3
{
    my %update1_pattern_map = (
        '^auth\s+required\s+pam_wheel\.so\s+use_uid\s*$' => 'auth            required        pam_wheel.so use_uid',
    );

    add_if_not_exist('/etc/pam.d/su', \%update1_pattern_map);    

    my %umask_pattern_map = (
        '^\s*umask (002)\s*$' => '007',
        '^\s*umask (022)\s*$' => '027',
    );

    update_if_not_exist('/etc/bashrc', \%umask_pattern_map, 'replace');
    update_if_not_exist('/etc/profile', \%umask_pattern_map, 'replace');

    my %add_pattern_map = (
        '^export\s+TMOUT\s*=\s*900\s*$' => 'export TMOUT=900',
    );

    add_if_not_exist('/etc/bashrc', \%add_pattern_map);
    add_if_not_exist('/etc/profile', \%add_pattern_map);
}

sub authen1
{
    my ($fname) = @_;

    my %update_pattern_map = (
        '^password\s+sufficient\s+pam_unix\.so\s+(.+)$' => ' remember=5',
    );

    update_if_not_exist($fname, \%update_pattern_map, 'append');

    my $pam_faillock_req = 'auth        required      pam_faillock.so preauth audit silent deny=5 unlock_time=900';
    my $pam_faillock_suf = 'auth        sufficient    pam_faillock.so authsucc audit deny=5 unlock_time=900';
    my $pam_faillock_die = 'auth        [default=die] pam_faillock.so authfail audit deny=5 unlock_time=900';
    my $pam_faillock_bad = 'auth        [success=1 default=bad] pam_unix.so';

    my %add_pattern_map = (
        '^auth\s+required\s+pam_faillock.so\s+preauth\s+.+$' => $pam_faillock_req,
        '^auth\s+sufficient\s+pam_faillock.so\s+authsucc\s+.+$' => $pam_faillock_suf,
        '^auth\s+\[default=die\]\s+pam_faillock.so\s+authfail\s+.+$' => $pam_faillock_die,
        '^auth\s+\[success=1 default=bad\]\s+pam_unix.so\s*$' => $pam_faillock_bad,
    );

    add_if_not_exist($fname, \%add_pattern_map);
}

sub passwd_policy
{
    my %pattern_map = (
        '^minlen\s+=\s+14\s*$' => 'minlen = 14',
        '^dcredit\s+=\s+-1\s*$' => 'dcredit = -1',
        '^ucredit\s+=\s+-1\s*$' => 'ucredit = -1',
        '^lcredit\s+=\s+-1\s*$' => 'lcredit = -1',
        '^ocredit\s+=\s+-1\s*$' => 'ocredit = -1',
    );

    add_if_not_exist('/etc/security/pwquality.conf', \%pattern_map);
}

sub sshd
{
    my $mac_setting = 'MACs hmac-sha2-512,hmac-sha2-256,hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,umac-128@openssh.com';

    my %pattern_map = (
        '^Protocol\s+2\s*$' => 'Protocol 2',
        '^ClientAliveInterval\s+900\s*$' => 'ClientAliveInterval 900',
        '^LogLevel\s+INFO\s*$' => 'LogLevel INFO',
        '^X11Forwarding\s+no\s*$' => 'X11Forwarding no',
        '^MaxAuthTries\s+4\s*$' => 'MaxAuthTries 4',
        '^PermitRootLogin\s+no\s*$' => 'PermitRootLogin no',
        '^LoginGraceTime\s+60\s*$' => 'LoginGraceTime 60',
        '^Banner\s+\/etc\/issue\.net\s*$' => 'Banner /etc/issue.net',
        '^MACs hmac-sha2-512,.*umac-128@openssh.com$' => $mac_setting,
    );

    add_if_not_exist('/etc/ssh/sshd_config', \%pattern_map);
}

sub sshd_adjust
{
    my %pattern_map = (
        '^X11Forwarding\s+(.+)\s*$' => 'no',
    );

    update_if_not_exist('/etc/ssh/sshd_config', \%pattern_map, 'replace');
}

sub cron
{
    execute_command('chmod 600 /etc/crontab');
    execute_command('chmod 600 /etc/cron.hourly/');
    execute_command('chmod 600 /etc/cron.daily/');
    execute_command('chmod 600 /etc/cron.weekly/');
    execute_command('chmod 600 /etc/cron.monthly/');
    execute_command('chmod 600 /etc/cron.d/');

    my $cfg_file = '/etc/cron.deny';

    if (-e $cfg_file) 
    {
        print("File [$cfg_file] already exist so delete it.\n");
        unlink($cfg_file);
    }
    else
    {
        print("File [$cfg_file] does not exist, so nothing to do.\n");
    }
}

sub rsys_log
{
    my %pattern_map = (
        '^\$FileCreateMode\s+0640\s*$' => '$FileCreateMode 0640',
    );

    add_if_not_exist('/etc/rsyslog.conf', \%pattern_map);
}

sub iptables_drop
{
    execute_command('iptables -A INPUT -i lo -j ACCEPT');
    execute_command('iptables -A OUTPUT -o lo -j ACCEPT');
    execute_command('iptables -A INPUT -s 127.0.0.0/8 -j DROP');
}

sub var_log_permission
{
    execute_command('find /var/log -type f -exec chmod g-wx,o-rwx {} +');
}

sub grub
{
    execute_command('chmod 600 /boot/grub2/grub.cfg');
}

sub fix_only_for_grub
{
    my ($fname, $pattern, $option) = @_;

    my $basename = basename($fname);
    my $backup_file = "$BACKUP_DIR/$basename";
    backup_file($fname, $backup_file);


    my @lines = file_to_array($fname);
    my @added_lines = ();

    my $found = 0;
    for my $line (@lines)
    {
        if ($line =~ /$pattern/) 
        {
            my $current_setting = $1;

            if ($current_setting =~ /$option/)
            {
                #Do nothing
                print("[$option] already exist, so do nothing.\n");
            }
            else
            {
                #Modify $line by Appending   
                my $new_setting = "GRUB_CMDLINE_LINUX=\"$current_setting $option\"\n";
                $line = $new_setting;         
            }
            
            $found++;
        }

        push(@added_lines, "$line");
    }

    if ($found <= 0)
    {
        #Added new line here
        my $setting = "GRUB_CMDLINE_LINUX=\"$option\"";

        print(">>> [$setting]\n");   
        push(@added_lines, "$setting\n");        
    }

    array_to_file($fname, \@added_lines);
}

sub grub_audit
{
    my $cfg = '/etc/default/grub';

    fix_only_for_grub($cfg, '^GRUB_CMDLINE_LINUX\s*=\s*"(.*)"\s*$', 'audit=1');

    execute_command('grub2-mkconfig -o /boot/grub2/grub.cfg');
}

sub fstab_update
{
    my %patterns = (
        '^.+\s+\/tmp\s+.+?\s+(.+?)\s+.+?\s+.+?\s*$'         => ',nodev,nosuid', #Still allow /tmp executable
        '^tmpfs\s+\/dev\/shm\s+.+?\s+(.+?)\s+.+?\s+.+?\s*$' => ',nodev,nosuid,noexec',
    );

    my $cfg = '/etc/fstab';
    update_if_not_exist($cfg, \%patterns, 'append');
}

sub fstab_add
{
    my %patterns = (
        '^tmpfs\s+\/dev\/shm\s+.+?\s+(.+?)\s+.+?\s+.+?\s*$' => 'tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0',
    );

    my $cfg = '/etc/fstab';
    add_if_not_exist($cfg, \%patterns);
}

sub core_dump
{
    my %pattern_map = (
        '^\*\s+hard\s+core\s+0\s*$' => '* hard core 0',
    );

    add_if_not_exist('/etc/security/limits.conf', \%pattern_map);

    my $dir = '/etc/security/limits.d/';
    opendir(my $dh, $dir);
    my @files = readdir($dh);
    closedir($dh);

    foreach my $file (@files)
    {
        next if($file =~ /^\.$/);
        next if($file =~ /^\.\.$/);

        add_if_not_exist("$dir/$file", \%pattern_map);
    }    
}

sub sysctl
{
    my %pattern_map = (
        '^net\.ipv4\.conf\.all\.accept_source_route\s*=\s*0\s*$' => 'net.ipv4.conf.all.accept_source_route = 0',
        '^net\.ipv4\.conf\.default\.accept_source_route\s*=\s*0\s*$' => 'net.ipv4.conf.default.accept_source_route = 0',
        '^net\.ipv4\.conf\.all\.log_martians\s*=\s*1\s*$' => 'net.ipv4.conf.all.log_martians = 1',
        '^net\.ipv4\.conf\.default\.log_martians\s*=\s*1\s*$' => 'net.ipv4.conf.default.log_martians = 1',

        '^net\.ipv4\.conf\.all\.send_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.all.send_redirects = 0',
        '^net\.ipv4\.conf\.default\.send_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.default.send_redirects = 0',
        '^net\.ipv4\.conf\.all\.accept_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.all.accept_redirects = 0',
        '^net\.ipv4\.conf\.default\.accept_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.default.accept_redirects = 0',
        '^net\.ipv4\.conf\.all\.secure_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.all.secure_redirects = 0',
        '^net\.ipv4\.conf\.default\.secure_redirects\s*=\s*0\s*$' => 'net.ipv4.conf.default.secure_redirects = 0',

        '^net\.ipv4\.ip_forward\s*=\s*0\s*$' => 'net.ipv4.ip_forward = 0',
    );

    add_if_not_exist('/etc/sysctl.conf', \%pattern_map);
}

sub sysctl_adjust
{
    my %pattern_map = (
        '^net\.ipv4\.conf\.all\.accept_source_route\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.default\.accept_source_route\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.all\.log_martians\s*=\s*(.+)\s*$' => '1',
        '^net\.ipv4\.conf\.default\.log_martians\s*=\s*(.+)\s*$' => '1',

        '^net\.ipv4\.conf\.all\.send_redirects\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.default\.send_redirects\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.all\.accept_redirects\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.default\.accept_redirects\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.all\.secure_redirects\s*=\s*(.+)\s*$' => '0',
        '^net\.ipv4\.conf\.default\.secure_redirects\s*=\s*(.+)\s*$' => '0',

        '^net\.ipv4\.ip_forward\s*=\s*(.+)\s*$' => '0',
    );

    update_if_not_exist('/etc/sysctl.conf', \%pattern_map, 'replace');

    #forced setting to take effect immediately
    execute_command('sysctl -p');    
}

sub audit_rule
{
    my $cfg_file = '/etc/audit/rules.d/audit.rules';
    my $content = <<'END_MESSAGE';

# audit rules
# replace file /etc/audit/rules.d/audit.rules
## First rule - delete all
-D

## Increase the buffers to survive stress events.
## Make this bigger for busy systems
-b 8192

## Set failure mode to syslog
-f 1

# audit time changes
-a always,exit -F arch=b64 -S adjtimex -S stime -S settimeofday -S clock_settime -k audit_time
-w /etc/localtime -p wa -k audit_time

# audit account changes
-w /etc/group -p wa -k audit_account
-w /etc/passwd -p wa -k audit_account
-w /etc/gshadow -p wa -k audit_account
-w /etc/shadow -p wa -k audit_account
-w /etc/security/opasswd -p wa -k audit_account

# audit hostname changes
-a always,exit -F arch=b64 -S sethostname -S setdomainname -k audit_hostname

# audit banner changes
-w /etc/issue -p wa -k audit_banner
-w /etc/issue.net -p wa -k audit_banner

# audit network changes
-w /etc/hosts -p wa -k audit_network
-w /etc/sysconfig/network -p wa -k audit_network
-w /etc/sysconfig/network-scripts/ -p wa -k audit_network

# audit selinux changes
-w /etc/selinux/ -p wa -k audit_selinux
-w /usr/share/selinux/ -p wa -k audit_selinux

# audit login
-w /var/log/lastlog -p wa -k audit_login
-w /var/run/faillock/ -p wa -k audit_login
-w /var/run/utmp -p wa -k audit_login
-w /var/log/wtmp -p wa -k audit_login
-w /var/log/btmp -p wa -k audit_login

# audit permission changes
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S chmod -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fchmod -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fchmodat -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S chown -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fchown -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fchownat -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S lchown -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S setxattr -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S lsetxattr -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fsetxattr -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S removexattr -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S lremovexattr -k audit_perm
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S fremovexattr -k audit_perm

# audit mount
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S mount -k audit_mount

# audit file changes
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -k audit_file
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -k audit_file
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S unlink -S unlinkat -S rename -S renameat -F exit=-EPERM -k audit_file
-a always,exit -F arch=b64 -F auid>=1000 -F auid!=4294967295 -S unlink -S unlinkat -S rename -S renameat -F exit=-ACCESS -k audit_file

# audit privilege escalation
-w /bin/su -p x -k audit_su
-w /usr/bin/sudo -p x -k audit_sudo
-w /etc/sudoers -p wa -k audit_sudo
-w /etc/sudoers.d/ -p wa -k audit_sudo
-w /var/log/sudo.log -p wa -k audit_sudo

# audit kernel module
-w /sbin/insmod -p x -k audit_kernel
-w /sbin/rmmod -p x -k audit_kernel
-w /sbin/modprobe -p x -k audit_kernel
-a always,exit -F arch=b64 -S init_module -S delete_module -k audit_kernel

# audit privileged command
-a always,exit -F path=/bin/ping6 -F perm=x -F auid>=1000 -F auid!=4294967295 -k audit_privileged_cmd
-a always,exit -F path=/bin/fusermount -F perm=x -F auid>=1000 -F auid!=4294967295 -k privileged
-a always,exit -F path=/bin/mount -F perm=x -F auid>=1000 -F auid!=4294967295 -k audit_privileged_cmd

# Make the configuration immutable
-e 2
END_MESSAGE

    my $basename = basename($cfg_file);
    my $backup_file = "$BACKUP_DIR/$basename";
    backup_file($cfg_file, $backup_file);

    create_text_file($cfg_file, $content);
    print("Replaced file [$cfg_file] with new content below\n");
    print("######## ===============================\n");
    print($content);
    print("######## ===============================\n");
}

#==== private here ====

sub get_shellable_users
{
    my @exceptions = ['root', 'ubuntu'];

    my $cfg_file = '/etc/passwd';
    my @lines = file_to_array($cfg_file);

    my @arr = ();
    for my $line (@lines) 
    {
        if ($line =~ /^(.+)\:(.+)\:(.+)\:(.+)\:(.*)\:(.*)\:(.*)$/) 
        {
            my $shell = $7;
            my $user = $1;
            my $home = $6;

            if ($user ~~ @exceptions)
            {
                print("Password expire exception for user [$user]\n");
                next;
            }

            #Only for bash shell
            #print("[$home] [$user] [$shell]\n");
            if ($shell =~ /^(.+)\/bash$/) 
            {
                push(@arr, $user);
            }
        }
    }

    return @arr;
}

sub parse_line
{
    my ($line, $pattern, $new_setting, $mode) = @_;

    my $new_line = $line;

    if ($line =~ /$pattern/) 
    {
        my $token = $1;

        if (index($token, $new_setting) != -1)
        {
            #print("Setting [$new_setting] already exist, so nothing to do.\n");
            return $new_line;
        }
        
        if ($mode eq 'append')
        {
            my $new_token = "$token$new_setting";
            $new_line =~ s/$token/$new_token/ig;

            #print("Changed line setting from [$token] to [$new_token].\n");
        }
        elsif ($mode eq 'replace')
        {
            my $new_token = "$new_setting";
            $new_line =~ s/$token/$new_token/ig;
        }        
    }

    return $new_line;
}

sub update_if_not_exist
{
    my ($fname, $pattern_map_ptr, $mode) = @_;
    my %pattern_map = %$pattern_map_ptr;

    my $basename = basename($fname);
    my $backup_file = "$BACKUP_DIR/$basename";
    backup_file($fname, $backup_file);

    my @lines = file_to_array($fname);
    my @added_lines = ();

    my @patterns = keys %pattern_map;

    for my $line (@lines)
    {
        my $new_line = $line;
        for my $pattern (@patterns) 
        {
            #Should match only 1 in the @patterns

            if ($line =~ /$pattern/) 
            {
                my $setting = $pattern_map{$pattern};
                $new_line = parse_line($line, $pattern, $setting, $mode);

                my $tmp_line = $line;
                my $tmp_new_line = $new_line;

                chomp($tmp_line);
                chomp($tmp_new_line);

                if ($new_line ne $line)
                {
                    print("<<< [$tmp_line]\n");
                    print(">>> [$tmp_new_line]\n");                       
                }
                else
                {
                    print("Setting [$setting] already exist, so nothing to do.\n");
                    print("=== [$tmp_line]\n"); 
                }
            }
        }

        push(@added_lines, $new_line);
    }

    array_to_file($fname, \@added_lines);
}

sub add_if_not_exist
{
    my ($fname, $pattern_map_ptr) = @_;
    my %pattern_map = %$pattern_map_ptr;

    my $basename = basename($fname);
    my $backup_file = "$BACKUP_DIR/$basename";
    backup_file($fname, $backup_file);

    my @lines = file_to_array($fname);
    my @added_lines = ();

    my @patterns = keys %pattern_map;
    for my $pattern (@patterns) 
    {
        my $cnt = 0;
        for my $line (@lines)
        {
            if ($line =~ /$pattern/) 
            {
                my $tmp_line = $line;
                chomp($tmp_line);
                print("Setting [$tmp_line] already exist, so nothing to do.\n");
                print("=== [$tmp_line]\n"); 

                $cnt++;
            }
        }

        #Tried every lines for each pattern
        if ($cnt <= 0)
        {
            my $setting = $pattern_map{$pattern};

            #No line match
            print(">>> [$setting]\n");   
            push(@added_lines, "$setting\n");
        }
    }

    push(@lines, @added_lines);

    array_to_file($fname, \@lines);
}

sub array_to_file
{
    my ($fname, $arr_ptr) = @_;
    my @arr = @$arr_ptr;

    open my $fh, '>', "$fname" or die "Cannot open $fname: $!";

    # Loop over the array
    foreach (@arr)
    {
        print($fh "$_");
    }

    close($fh);
}

sub file_to_array
{
    my ($fname) = @_;

    my @arr = ();

    open(my $fh, '<', $fname) or die "Could not open file '$fname' $!";
    while (my $line = <$fh>) 
    {
        push(@arr, $line);
    } 

    close($fh);  
    return @arr;
}

sub backup_file
{
    my ($oldfile, $newfile) = @_;

    print("Backing up [$oldfile] to [$newfile]...\n");
    copy("$oldfile", "$newfile") or die "Copy failed: $!";
}

sub create_text_file
{
    my ($fname, $content) = @_;

    open(my $fh, '>', $fname) or die "Could not open file '$fname' $!";
    print($fh $content);
    close($fh);
}

sub execute_command
{
    my ($cmd) = @_;

    print("Executing command [$cmd] ...\n");
    my $err_msg = system("$cmd");
    my $retcode = ($? >> 8);

    if ($retcode != 0)
    {
        print("Execute command [$cmd] failed!!!\n");
        return;
    }    
}